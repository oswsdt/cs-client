﻿/*
 * User: Lukas
 * Date: 23.04.2020
 * Time: 22:42
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

namespace cs_client
{
	public class cs_client : ServiceBase
	{
		public const string MyServiceName = "cs_client";
		
		public cs_client()
		{
			InitializeComponent();
		}
		
		private void InitializeComponent()
		{
			this.ServiceName = MyServiceName;
		}
		
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
		
		protected override void OnStart(string[] args)
		{
			this.process();
		}
		
		//protected override void OnStop()
		//{
			// TODO: Add tear-down code here (if required) to stop your service.
		//}
		
		public void process()
		{
			
		}
	}
}
