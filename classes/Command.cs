﻿using System;
using System.Collections.Generic;

namespace cs_client.classes
{
	public class Command
	{
		public static void sendGeneralInfoToServer()
		{
			
		}
		
		public static void sendStatusInfoToServer()
		{
			
		}
		
		public static void requestSoftwareRepository()
		{
			
		}
		
		public static void handle(Dictionary<string, string> data)
		{
			
		}
	}
	
	public static class ServerToClientCommand
	{
		public const int SET_STATUS_ON_CLIENT = 0;
		public const int RET_SOFTWARE_REPOSITORY = 1;
	}
	
	public static class ClientToServerCommand
	{
		public const int SEND_GENERAL_INFO_TO_SERVER = 0;
		public const int SEND_STATUS_INFO_TO_SERVER = 1;
		public const int GET_SOFTWARE_REPOSITORY = 2;
	}
}
