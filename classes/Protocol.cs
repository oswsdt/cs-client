﻿using System;

namespace cs_client.classes
{
	public class Protocol
	{
		public const int PROTOCOL_HEADER_LENGTH = 10;
		public const int PROTOCOL_TERMINATION_LENGTH = 1;
		public const int PROTOCOL_COBS_LENGTH = 2;
		public const int PROTOCOL_CHECKSUM_LENGTH = 2;
		public const int PROTOCOL_LENGTH_LENGTH = 2;
		public const int MAX_DATA_LENGTH = 65536 - PROTOCOL_HEADER_LENGTH - PROTOCOL_CHECKSUM_LENGTH;  //2^16 - header - checksum
		public const int MAX_PACKAGE_LEN = 65536 + PROTOCOL_COBS_LENGTH + PROTOCOL_TERMINATION_LENGTH;  // 2^16 + cobs + termination
		public const int VERSION = 0; 
	}
}
