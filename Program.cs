﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace cs_client
{
	static class Program
	{
		static void Main()
		{
			//START SERVICE
			
			//use following for release:
			//ServiceBase.Run(new ServiceBase[] { new cs_client() });
			
			//use for debugging:
			cs_client client = new cs_client();
			client.process();
		}
	}
}
